<%@ page import="org.datafy.agile.customer.Customer" %>
<%@ page import="java.util.List" %>
<%@include file="head.html"%>
<title> Edit: ${lastName}, ${firstName} | Datafy </title>
<%@include file="header.html"%>
<body>
<main role="main" class="container">
    <h1 class="mt-5" style="padding-bottom: 35px;">
        <div style="text-align: center;">
            Edit Customer : ${lastName}, ${firstName}</h1>
    </div>
    <div class="row" style="padding-bottom: 15px;">
        <div class="col">
            <img src="images/edit_person.png">

        </div>
        <div class="col">
            <form method="post" action="edit?id=${id}">

                <div class="form-group" style="width: 75%;" >
                    <div style="padding-bottom: 25px;">
                        <label for="firstName">First Name</label>
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Please insert a firstname..." value="${firstName}">

                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Please insert a lastname..." value="${lastName}">

                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Please insert an Email-Address..." value="${email}">

                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Please give an address..." value="${address}">

                        <label for="phoneNumber">Phone Number</label>
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Please insert the postal code..." value="${phoneNumber}"><br>
                        <p>All the above fields are required!</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>


        </div>
    </div>
    <c:choose>
        <c:when test="${isPost}">
            <div style="text-align: center; padding-bottom: 15px;">
                <h4 style="color: ${color}">${showNot}</h4>
            </div>
        </c:when>
    </c:choose>
</main>
</body>
<%@include file="footer.html"%>
