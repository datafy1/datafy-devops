<%--
  Created by IntelliJ IDEA.
  User: kevin
  Date: 13.01.23
  Time: 09:22
  To change this template use File | Settings | File Templates.
--%>
<%@include file="head.html"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title> Search customer | Datafy </title>
<%@include file="header.html"%>
<body>
<c:choose>
<c:when test="${result eq false}">
<main role="main" class="container">
    <div class="mt-5" style="padding-bottom: 15px;">
        <div style="text-align: center;">
            <div style="text-align: center; padding-bottom: 15px;">
                <h4 style="color: ${color}">${showNot}</h4>
            </div>
        </div>
    </div>
</main>
</c:when>
<c:otherwise>
<main role="main" class="container">
    <h1 class="mt-5" style="padding-bottom: 15px;">Search Results :</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 8px;">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scope="col">Phone Number</th>
            <th scope="col" style="width: 10px;">Edit Customer </th>
            <th scope="col" style="width: 10px;">Delete Customer </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="c" items="${customer}">
            <th scope="row">${c.id} </th>
            <td>${c.getFirstName()}</td>
            <td>${c.getLastname()}</td>
            <td>${c.getEmail()}</td>
            <td>${c.getAddress()}</td>
            <td>${c.getPhoneNumber()}</td>
            <td><a href="edit?id=${c.getId()}"> <img src="images/edit.png"></a></td>
            <td><a href="delete?id=${c.getId()}"> <img src="images/delete.png"></a></td>
        </c:forEach>
        </tbody>
    </table>
</main>
</c:otherwise>
</c:choose>
<%@include file="footer.html"%>

