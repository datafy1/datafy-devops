<%--
  Created by IntelliJ IDEA.
  User: kevin
  Date: 12.01.23
  Time: 19:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="head.html"%>
<title> Add Customer | Datafy </title>
<body>
<%@include file="header.html"%>
<main role="main" class="container">
    <h1 class="mt-5" style="padding-bottom: 35px;">
        <div style="text-align: center;">
            Add a new Customer</h1>
    </div>
    <div class="row" style="padding-bottom: 15px;">
        <div class="col">
            <img src="images/add.png">

        </div>
        <div class="col">
            <form method="post" action="add">

                <div class="form-group" style="width: 75%;" >
                    <div style="padding-bottom: 25px;">
                        <label for="firstName">First Name</label>
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Please insert a firstname...">

                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Please insert a lastname...">

                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Please insert an Email-Address...">

                        <label for="street">Street & Street No.</label>
                        <input type="text" class="form-control" id="street" name="streetAndNumber" placeholder="Please insert the name of the street...">

                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Please insert the name of the city...">

                        <label for="postalCode">Postal Code</label>
                        <input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Please insert the postal code..." pattern="[0-9]{5}" title="Five digits only!">

                        <label for="phoneNumber">Phone Number</label>
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Please insert the phone number..."><br>
                        <p>All the above fields are required!</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <c:set var="isPost" value="${isPost}"/>
    <c:set var="color" value="${color}"/>
    <c:set var="showNot" value="${showNot}"/>
    <c:choose>
        <c:when test="${isPost}">
            <div style="text-align: center; padding-bottom: 15px;">
                <h4 style="color: ${color}">${showNot}</h4>

            </div>
        </c:when>
    </c:choose>
</main>
<%@include file="footer.html"%>
