<%@ page import="org.datafy.agile.customer.Customer" %>
<%@ page import="java.util.List" %>
<%@ page import="org.datafy.agile.customer.Customer" %>
<%@include file="head.html"%>
<title> Home | Datafy </title>
<%@include file="header.html"%>
<body>
<main role="main" class="container">
    <h1 class="mt-5" style="padding-bottom: 15px;">Clients</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col" style="width: 8px;">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scope="col">Phone Number</th>
            <th scope="col" style="width: 10px;">Edit Customer </th>
            <th scope="col" style="width: 10px;">Delete Customer </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Customer> customerList = (List) request.getAttribute("customerList");
            for (Customer c : customerList ) {
        %>
        <tr>
            <th scope="row"><%=c.getId()%></th>
            <td><%=c.getFirstName()%></td>
            <td><%=c.getLastname()%></td>
            <td><%=c.getEmail()%></td>
            <td><%=c.getAddress()%></td>
            <td><%=c.getPhoneNumber()%></td>
            <td><a href="edit?id=<%=c.getId()%>"> <img src="images/edit.png"></a></td>
            <td><a href="delete?id=<%=c.getId()%>"> <img src="images/delete.png"></a></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</main>
</body>
<%@include file="footer.html"%>
