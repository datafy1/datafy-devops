package org.datafy.agile;
import org.datafy.agile.config.DBConnect;
import org.datafy.agile.customer.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchServlet extends HttpServlet {

    private List<Customer> customerList = new ArrayList<>();
    String showNotification;
    String search;
    String color;
    String redHtml = "#bd2130";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        customerList.clear();
        showNotification = "Invalid operation!";
        color = redHtml;
        request.setAttribute("color", color);
        request.setAttribute("showNot", showNotification);
        request.getRequestDispatcher("/search.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        customerList.clear();
        search = request.getParameter("search");
        String searchCapitalized = search.substring(0, 1).toUpperCase() + search.substring(1);
        request.setAttribute("result", searchForCustomer(searchCapitalized));
        request.setAttribute("customer", customerList);
        request.setAttribute("showNot", showNotification);
        request.setAttribute("color", color);
        request.getRequestDispatcher("/search.jsp").forward(request, response);
    }

    protected boolean searchForCustomer(String searchCapitalized) {
        boolean results = false;
        PreparedStatement st = null;
        String searchSql = "SELECT * from accounts WHERE firstName LIKE ? OR lastName LIKE ?";
        try {
            st = DBConnect.getExternalConnection().prepareStatement(searchSql);
            st.setString(1, searchCapitalized);
            st.setString(2, searchCapitalized);
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()){
                color = redHtml;
                showNotification = "No results for the search query : "+search;
            }else{
                int id = resultSet.getInt("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                String email = resultSet.getString("email");
                String address = resultSet.getString("address");
                String phoneNumber = resultSet.getString("phonenumber");
                Customer customerFound = new Customer(id, firstname, lastname, email, address, phoneNumber);
                customerList.add(customerFound);
                results = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnect.closeStatement(st);
            DBConnect.closeConnection();
        }
        return results;
    }
}
