package org.datafy.agile.config;

public class DBConfig {
    private static final String TYPE = "sqlite";
    private static final String DB_PATH = System.getenv("DB_PATH");

    public static String getType() {return TYPE; }
    public static String getDbPath() {return DB_PATH; }
}

