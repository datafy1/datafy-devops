package org.datafy.agile.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBConnect {

    public static Connection getExternalConnection() {
        String jdbcUrl = "jdbc:"+DBConfig.getType()+":"+DBConfig.getDbPath();
        try {
            return DriverManager.getConnection(jdbcUrl);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void closeStatement(PreparedStatement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void closeConnection() {
        try {
            if (DBConnect.getExternalConnection() != null){
                DBConnect.getExternalConnection().close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

