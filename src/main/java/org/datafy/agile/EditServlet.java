package org.datafy.agile;

import org.datafy.agile.config.DBConnect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EditServlet extends HttpServlet {
    boolean isPost;
    String firstName;
    String lastName;
    String email;
    String allTheAddress;
    String phoneNumber;
    String showNotification;
    String color;
    String redHtml = "#bd2130";
    String greenHtml = "#1c7430";
    int id;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        isPost = false;
        id = Integer.parseInt(req.getParameter("id"));

        retrieveCustomerData(id);
        req.setAttribute("id", id);
        req.setAttribute("firstName", firstName);
        req.setAttribute("lastName", lastName);
        req.setAttribute("email", email);
        req.setAttribute("address", allTheAddress);
        req.setAttribute("phoneNumber", phoneNumber);
        req.setAttribute("isPost", isPost);
        req.getRequestDispatcher("/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        isPost = true;
        color = null;
        showNotification = "";
        firstName = req.getParameter("firstName");
        lastName = req.getParameter("lastName");
        email = req.getParameter("email");
        allTheAddress = req.getParameter("address");
        phoneNumber = req.getParameter("phoneNumber");

        if (email.equals("") || firstName.equals("") || lastName.equals("") || phoneNumber.equals("") || allTheAddress.equals("")){
            showNotification = "All the fields are required!";
            color=redHtml;
        }else{
            updateSQL(firstName,lastName,email,allTheAddress,phoneNumber,id);
        }

        retrieveCustomerData(id);
        req.setAttribute("id", id);
        req.setAttribute("firstName", firstName);
        req.setAttribute("lastName", lastName);
        req.setAttribute("email", email);
        req.setAttribute("address", allTheAddress);
        req.setAttribute("phoneNumber", phoneNumber);
        req.setAttribute("isPost", isPost);
        req.setAttribute("showNot", showNotification);
        req.setAttribute("isPost", isPost);
        req.setAttribute("color", color);
        req.getRequestDispatcher("/edit.jsp").forward(req, resp);
    }

    private void updateSQL(String firstName, String lastName, String email, String allTheAddress, String phoneNumber,int id) {
        int i = 0;

        String updateSql = "UPDATE accounts SET firstname=?, lastname=?,email=?,address=?,phonenumber=? WHERE id=?";

        try {
            PreparedStatement st = DBConnect.getExternalConnection().prepareStatement(updateSql);
            st.setString(1,firstName);
            st.setString(2,lastName);
            st.setString(3,email);
            st.setString(4,allTheAddress);
            st.setString(5,phoneNumber);
            st.setInt(6,id);
            i = st.executeUpdate();
            if (i > 0){
                showNotification = "The data was saved on our database! Thank you!";
                color = greenHtml;
            }else{
                showNotification = "The data was not saved successfully! Please try again!";
                color = redHtml;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (DBConnect.getExternalConnection() != null) {
                    DBConnect.getExternalConnection().close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void retrieveCustomerData(int id) {
        String select = "SELECT * from accounts WHERE id=?";
        try {
            PreparedStatement st = DBConnect.getExternalConnection().prepareStatement(select);
            st.setInt(1,id);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()){
                firstName = resultSet.getString("firstname");
                lastName = resultSet.getString("lastname");
                email = resultSet.getString("email");
                allTheAddress = resultSet.getString("address");
                phoneNumber = resultSet.getString("phonenumber");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            showNotification = "Couldn't find any customer with the given id!";
            color = redHtml;
        } finally {
            try {
                if (DBConnect.getExternalConnection() != null) {
                    DBConnect.getExternalConnection().close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
