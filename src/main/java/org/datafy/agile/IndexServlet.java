package org.datafy.agile;

import org.datafy.agile.config.DBConnect;
import org.datafy.agile.customer.Customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IndexServlet extends HttpServlet {
    String showError;
    private List<Customer> customerList = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        retrieveCustomers();
        request.setAttribute("customerList", customerList);

        request.getRequestDispatcher("index.jsp").forward(request, response);
        customerList.clear();
    }

    private void retrieveCustomers() {
        PreparedStatement st = null;
        String query = "SELECT * from accounts";
        try {
            st = DBConnect.getExternalConnection().prepareStatement(query);
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                showError = "No data available";
            } else {
                do {
                    int id = Integer.parseInt(resultSet.getString("id"));
                    String firstname = resultSet.getString("firstname");
                    String lastname = resultSet.getString("lastname");
                    String email = resultSet.getString("email");
                    String address = resultSet.getString("address");
                    String phoneNumber = resultSet.getString("phonenumber");
                    customerList.add(new Customer(id, firstname, lastname, email, address, phoneNumber));
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBConnect.closeStatement(st);
            DBConnect.closeConnection();
        }

    }
}
