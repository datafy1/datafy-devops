package org.datafy.agile;

import org.datafy.agile.config.DBConnect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeleteServlet extends HttpServlet {
    boolean isGet;
    int customerId;
    String showNotification;
    String color;
    String redHtml = "#bd2130";
    String greenHtml = "#1c7430";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        isGet = true;
        customerId = Integer.parseInt(request.getParameter("id"));
        if (validCustomer(customerId)){
            deleteCustomer(customerId);
        }
        request.setAttribute("color", color);
        request.setAttribute("showNot", showNotification);
        request.getRequestDispatcher("/delete.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        isGet = false;
        showNotification = "Invalid action, please go to the home page or click <a href=\"home\">here</a>";
        color = redHtml;
        request.getRequestDispatcher("/edit.jsp").forward(request, response);
    }

    protected void deleteCustomer(int id) {
        PreparedStatement st = null;
        String deleteStatement = "DELETE FROM accounts WHERE id=?";
        try {
            st = DBConnect.getExternalConnection().prepareStatement(deleteStatement);
            st.setInt(1,id);
            int updatedTable = st.executeUpdate();
            notifyUser(updatedTable);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnect.closeStatement(st);
            DBConnect.closeConnection();
        }
    }

    private boolean validCustomer(int id) {
        boolean isValid = false;
        PreparedStatement st = null;
        String query = "SELECT id FROM accounts WHERE id=?";
        try {
            st = DBConnect.getExternalConnection().prepareStatement(query);
            st.setInt(1,id);
            ResultSet resultSet = st.executeQuery();
            if(!resultSet.next()){
                showNotification = "Customer with ID "+id+" was not found on our database!";
                color = redHtml;
            }else{
                isValid = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnect.closeStatement(st);
            DBConnect.closeConnection();
        }
        return isValid;
    }

    private void notifyUser(int updatedTable) {
        if (updatedTable > 0){
            showNotification = "Customer was successfully deleted!";
            color = greenHtml;
        }else{
            showNotification = "Customer was not successfully deleted, please try again!";
            color = redHtml;
        }
    }
}

