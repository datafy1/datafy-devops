package org.datafy.agile;

import org.datafy.agile.config.DBConnect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddCustomerServlet extends HttpServlet {
    String firstName;
    String lastName;
    String email;
    String streetAndNo;
    String city;
    String completeAddress;
    String postalCode;
    String phoneNumber;
    String showNotification;
    boolean isPost;
    String color;
    String redHtml = "#bd2130";
    String greenHtml = "#1c7430";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        isPost = false;
        request.setAttribute("isPost", isPost);
        request.getRequestDispatcher("add.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        isPost = true;
        color = null;
        showNotification = "";
        firstName = request.getParameter("firstName");
        lastName = request.getParameter("lastName");
        email = request.getParameter("email");
        streetAndNo = request.getParameter("streetAndNumber");
        city = request.getParameter("city");
        postalCode = request.getParameter("postalCode");
        phoneNumber = request.getParameter("phoneNumber");

        if (allFieldsCompleted(firstName, lastName, email, streetAndNo, city, postalCode, phoneNumber)) {
            completeAddress = streetAndNo+", "+city+" "+postalCode;

            insertSQL(firstName,lastName,email,completeAddress,phoneNumber);
        } else {
            showNotification = "All the fields are required!";
            color=redHtml;
        }
        request.setAttribute("showNot", showNotification);
        request.setAttribute("isPost", isPost);
        request.setAttribute("color", color);
        request.getRequestDispatcher("/add.jsp").forward(request, response);
    }


    private boolean allFieldsCompleted(String firstName, String lastName, String email, String streetAndNo, String city, String postalCode, String phoneNumber) {
        return !streetAndNo.equals("") && !city.equals("") && !postalCode.equals("") && !email.equals("") && !firstName.equals("") && !lastName.equals("") && !phoneNumber.equals("");
    }

    private void insertSQL(String firstName, String lastName, String email, String completeAddress, String phoneNumber) {
        // updatedTable > 0 = no. of rows updated or 0 = not successful
        int updatedTable;
        String insertSql = "INSERT INTO accounts (firstname,lastname,email,address,phonenumber) VALUES(?,?,?,?,?)";
        if (!emailExists(email)){
            try {
                PreparedStatement st = DBConnect.getExternalConnection().prepareStatement(insertSql);
                st.setString(1,firstName);
                st.setString(2,lastName);
                st.setString(3,email);
                st.setString(4,completeAddress);
                st.setString(5,phoneNumber);
                updatedTable = st.executeUpdate();
                notifyUser(updatedTable);
            } catch (SQLException e) {
                e.printStackTrace();
                showNotification = "Error on our database! Please try again!";
                color = redHtml;
            } finally {
                if (DBConnect.getExternalConnection() != null) {
                    try {
                        DBConnect.getExternalConnection().close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            showNotification = "This email is already registered!";
            color = redHtml;
        }
    }

    private void notifyUser(int updatedTable) {
        if (updatedTable > 0){
            showNotification = "The data was saved on our database! Thank you!";
            color = greenHtml;
        } else {
            showNotification = "The data was not saved successfully! Please try again!";
            color = redHtml;
        }
    }

    private boolean emailExists(String email) {
        boolean itExists = true;
        String checkEmailStatement = "SELECT * from accounts where email=?";
        try {
            PreparedStatement st = DBConnect.getExternalConnection().prepareStatement(checkEmailStatement);
            st.setString(1, email);
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()){
                itExists = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (DBConnect.getExternalConnection() != null) {
                    DBConnect.getExternalConnection().close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return itExists;
    }
}


