import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAddCustomer {
    static ChromeOptions options = new ChromeOptions();
    static WebDriver driver;
    static String hostUrl = System.getenv("HOST_URL");
    public static String idToDelete = "";

    @Test(groups={"TestAddCustomer.addCustomer"})
    public static void testShouldAddCustomer() {
        options.addArguments("headless");
        driver = new ChromeDriver(options);
        driver.get(hostUrl+"add");
        addInputText("Kathrin", 1);
        addInputText("Nadel", 2);
        addInputText("KathrinNadel@teleworm.us", 3);
        addInputText("Paderborner Strasse 88", 4);
        addInputText("Gessertshausen", 5);
        addInputText("86459", 6);
        addInputText("08238438419", 7);

        driver.findElement(By.xpath("/html/body/main/div/div[2]/form/div/button")).click();

        driver.get(hostUrl+"home");

        int lastRowCount=driver.findElement(By.xpath("/html/body/main/table/tbody")).findElements(By.tagName("tr")).size();
        String firstName = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/td[1]")).getText();
        String lastName = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/td[2]")).getText();
        String emailAddress = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/td[3]")).getText();
        String address = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/td[4]")).getText();
        String phoneNumber = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/td[5]")).getText();
        idToDelete = driver.findElement(By.xpath("/html/body/main/table/tbody/tr["+lastRowCount+"]/th")).getText();

        Assert.assertEquals(firstName, "Kathrin");
        Assert.assertEquals(lastName, "Nadel");
        Assert.assertEquals(emailAddress, "KathrinNadel@teleworm.us");
        Assert.assertEquals(address, "Paderborner Strasse 88, Gessertshausen 86459");
        Assert.assertEquals(phoneNumber, "08238438419");
    }

    private static void addInputText(String textToType, int inputId){
        driver.findElement(By.xpath("/html/body/main/div/div[2]/form/div/div/input["+inputId+"]")).sendKeys(textToType);
    }
}

