import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestDelete {
    static ChromeOptions options = new ChromeOptions();
    static WebDriver driver;
    static String hostUrl = System.getenv("HOST_URL");

    @Test(dependsOnGroups = {"TestAddCustomer.addCustomer"})
    public static void testShouldDeleteCustomer() {
        options.addArguments("headless");
        driver = new ChromeDriver(options);
        driver.get(hostUrl+"home");
        String urlToDelete = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[5]/td[6]/a")).getAttribute("href");
        driver.get(urlToDelete);


        String notification = driver.findElement(By.xpath("/html/body/main/div/div/div/h4")).getText();

        Assert.assertEquals(notification, "Customer was successfully deleted!");
    }

    @Test
    public static void testShouldNotDeleteCustomerWithFalseId() {
        driver.get(hostUrl+"delete?id=0");
        String notificationShown = driver.findElement(By.xpath("/html/body/main/div/div/div/h4")).getText();

        Assert.assertEquals(notificationShown, "Customer with ID 0 was not found on our database!");

    }
}

