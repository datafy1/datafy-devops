import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSearch {
    static WebDriver driver = new ChromeDriver();
    static String hostUrl = System.getenv("HOST_URL");

    @Test
    public static void testShouldFindExistingCustomer() {
        driver.get(hostUrl+"home");
        driver.findElement(By.xpath("/html/body/header/nav/div/form/input")).sendKeys("Birgit");
        driver.findElement(By.xpath("/html/body/header/nav/div/form/button")).click();


        String foundFirstNameFromSearch = driver.findElement(By.xpath("/html/body/main/table/tbody/tr/td[1]")).getText();

        Assert.assertEquals(foundFirstNameFromSearch, "Birgit");
    }

    @Test
    public static void testShouldNotFindNonExistingCustomer() {
        driver.get(hostUrl+"home");
        driver.findElement(By.xpath("/html/body/header/nav/div/form/input")).sendKeys("Anna");
        driver.findElement(By.xpath("/html/body/header/nav/div/form/button")).click();


        String notificationShown = driver.findElement(By.xpath("/html/body/main/div/div/div/h4")).getText();

        Assert.assertEquals(notificationShown, "No results for the search query : Anna");
    }
}

