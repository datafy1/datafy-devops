import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Random;

public class TestEditCustomer {
    static WebDriver driver = new ChromeDriver();
    static String hostUrl = System.getenv("HOST_URL");

    @BeforeClass
    public void before(){

    }

    @Test
    public static void testShouldEditFirstCustomerPhoneNumber() {
        driver.get(hostUrl+"home");
        String urlToEdit = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[6]/a")).getAttribute("href");
        driver.get(urlToEdit);

        String phoneNumberGenerated = generatePhoneNumber();
        driver.findElement(By.xpath("/html/body/main/div/div[2]/form/div/div/input[5]")).clear();
        driver.findElement(By.xpath("/html/body/main/div/div[2]/form/div/div/input[5]")).sendKeys(phoneNumberGenerated);

        driver.findElement(By.xpath("/html/body/main/div/div[2]/form/div/button")).submit();

        driver.get(hostUrl+"home");

        String newNumber = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[5]")).getText();
        Assert.assertEquals(newNumber, phoneNumberGenerated);

    }

    private static String generatePhoneNumber(){
        // from http://www.javaproblems.com/2013/12/generating-phone-number-in-java.html
        Random generator = new Random();
        int one = generator.nextInt(8);
        int two = generator.nextInt(8);
        int three = generator.nextInt(8);
        int fourtosix = generator.nextInt(743);
        int seventoten = generator.nextInt(10000);
        String sOne = "" + (one);
        String sTwo = "" + (two);
        String sThree = "" + (three);
        String sFourtosix = "";
        String sSeventoten = "";
        if (fourtosix < 10)
            sFourtosix = "00" + (fourtosix);
        else
        if(fourtosix < 100)
            sFourtosix = "0" + (fourtosix);
        else
            sFourtosix = "" + (fourtosix);
        if (seventoten < 10)
            sSeventoten = "000" + (seventoten);
        else
        if(seventoten < 100)
            sSeventoten = "00" + (seventoten);
        else
        if(seventoten < 1000)
            sSeventoten = "0" + (seventoten);
        else
            sSeventoten = "" + (seventoten);

        return sOne + sTwo + sThree + sFourtosix + sSeventoten;
    }
}
