import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestIndex {
    static ChromeOptions options = new ChromeOptions();
    static WebDriver driver;
    static String hostUrl = System.getenv("HOST_URL");
    @BeforeClass
    public void before(){
        options.addArguments("headless");
        driver = new ChromeDriver(options);
        driver.get(hostUrl+"home");
    }
    @Test
    public static void testFirstCustomer() {
        String firstNameToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[1]")).getText();
        String lastNameToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[2]")).getText();
        String emailToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[3]")).getText();
        String addressToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[4]")).getText();
        String phoneNumberToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[5]")).getText();
        String deleteUrlToTest = driver.findElement(By.xpath("/html/body/main/table/tbody/tr[1]/td[6]/a")).getAttribute("href");

        Assert.assertEquals(firstNameToTest, "Birgit");
        Assert.assertEquals(lastNameToTest, "Bauer");
        Assert.assertEquals(emailToTest, "BirgitBauer@rhyta.com");
        Assert.assertEquals(addressToTest, "Ziegelstr. 7, 84329 Wurmannsquick");
        Assert.assertEquals(phoneNumberToTest, "8572702304");
        Assert.assertEquals(deleteUrlToTest, hostUrl+"delete?id=1");
    }

    @Test
    public static void testMenuItems() {
        String menuItemHome = driver.findElement(By.xpath("/html/body/header/nav/div/ul/li[1]/a")).getText();
        String addCustomerItem = driver.findElement(By.xpath("/html/body/header/nav/div/ul/li[2]/a")).getText();

        Assert.assertEquals(menuItemHome, "Home");
        Assert.assertEquals(addCustomerItem, "Add Customer");
    }
}
