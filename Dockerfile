# FROM maven:3.6.3-openjdk-17
# COPY target/Datafy-Agile.jar /app/application.jar
# ENV HOST_URL=url_on_azure
# ENV DB_PATH=src/main/java/org/datafy/agile/config/db/db.db
# ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar /app/application.jar

FROM tomcat:8.5.84-jre17

COPY target/Datafy-Agile.war /usr/local/tomcat/webapps/
COPY src/main/java/org/datafy/agile/config/db/db.db /tmp/db.db
COPY target/Datafy-Agile/WEB-INF/lib/javax.servlet-jstl-1.2.jar /usr/local/tomcat/lib/
COPY target/Datafy-Agile/WEB-INF/lib/jstl-jstl-1.2.jar /usr/local/tomcat/lib/
COPY target/Datafy-Agile/WEB-INF/lib/sqlite-jdbc-3.39.4.1.jar /usr/local/tomcat/lib/
CMD ["catalina.sh", "run"]